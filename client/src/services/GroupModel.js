import Api from '@/services/Api'

export default {

  all () {
    return Api().get('groups')
  },

  insert(fields){
    return Api().post('groups', fields);
  },

  find(id){
    return Api().get('groups/' + id);
  },

  update(id, fields){
    return Api().post('groups/' + id, fields);
  },

  delete(id){
    return Api().delete('groups/' + id);
  },

  deleteUser(id, user_id){
    return Api().delete('groups/' + id + '/users/' + user_id);
  }

}
