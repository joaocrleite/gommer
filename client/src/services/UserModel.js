import Api from '@/services/Api'

export default {

  all () {
    return Api().get('users')
  },

  insert(fields){
    return Api().post('users', fields);
  },

  find(id){
    return Api().get('users/' + id);
  },

  update(id, fields){
    return Api().post('users/' + id, fields);
  },

  delete(id){
    return Api().delete('users/' + id);
  }

}
