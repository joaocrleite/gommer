import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Register from '@/components/Register'

import UserList from '@/components/users/index'
import UserCreate from '@/components/users/create'
import UserEdit from '@/components/users/edit'

import GroupList from '@/components/groups/index'
import GroupEdit from '@/components/groups/edit'
import GroupUsers from '@/components/groups/users'

import RestaurantList from '@/components/restaurants/index'
import RestaurantMenu from '@/components/restaurants/menu'

Vue.use(Router)


export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/users',
      name: 'users',
      component: UserList
    },
    {
      path: '/users/create',
      name: 'users.create',
      component: UserCreate
    },
    {
      path: '/users/:id/edit',
      name: 'users.edit',
      component: UserEdit,
      props: true
    },
    {
      path: '/groups',
      name: 'groups',
      component: GroupList
    },
    {
      path: '/groups/:id/edit',
      name: 'groups.edit',
      component: GroupEdit,
      props: true
    },
    {
      path: '/groups/:id/users',
      name: 'groups.users',
      component: GroupUsers,
      props: true
    },
    {
      path: '/restaurants',
      name: 'restaurants',
      component: RestaurantList,
    },
    {
      path: '/restaurants/:id/menu',
      name: 'restaurants.view',
      component: RestaurantMenu,
      props: true
    },
  ]
})
