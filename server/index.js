const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('combine'));
app.use(cors());

const UserController = require('./controllers/UserController');
const GroupController = require('./controllers/GroupController');

/* Routes~Controllers */
UserController(app);
GroupController(app);
/* Routes~Controllers */

app.listen(8081);

console.log("Starting Server at port 8081");