'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    age: DataTypes.INTEGER
  }, {});
  User.associate = function(models) {
      User.belongsToMany(models.Group, {as: 'groups',  through: 'users_has_groups', foreignKey: 'user_id' , otherKey : 'group_id'});
  };
  return User;
};