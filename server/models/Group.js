'use strict';
module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    name: DataTypes.STRING
  }, {});
  Group.associate = function(models) {
    Group.belongsToMany(models.User, {as: 'users', through: 'users_has_groups', foreignKey: 'group_id' , otherKey : 'user_id'});
  };
  return Group;
};