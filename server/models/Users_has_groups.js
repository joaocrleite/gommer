'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users_has_groups = sequelize.define('Users_has_groups', {
    user_id: DataTypes.INTEGER,
    group_id: DataTypes.INTEGER
  }, {});
  Users_has_groups.associate = function(models) {
    
  };

  Users_has_groups.removeAttribute('id');

  return Users_has_groups;
};