const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const app = express();
app.use(morgan('combine'));
app.use(bodyParser.json());
app.use(cors());


const UserController = require('./controllers/UserController');
const GroupController = require('./controllers/GroupController');

/* Routes~Controllers */
UserController(app);
GroupController(app);
/* Routes~Controllers */

app.listen(process.env.PORT || 8081);


console.log("Starting Server at port 8081");