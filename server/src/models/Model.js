
const db = require('../services/db');

modules.exports = {
    table = null,

    all(callback) {
        db.all(this.table, (err, rows, fields) => {
            callback(err, rows, fields);
        });
    },

    insert(fields, callback){

        db.insert(this.table, fields, (err, result) => {
            callback(err, result);
        });
    
    },

    find(id, callback){

        db.find(this.table, id, (err, result) => {
            if(result.length > 0){
                callback(err, result[0]);
            }else{
                callback(err, null);
            }
           
        });
    
    },

    update(id, fields, callback){

        db.update(this.table, id,fields, (err, result) => {
            callback(err, result);
        });
    
    },

    remove(id, callback){

        db.remove(this.table, id, (err, result) => {
            callback(err, result);
        });
    
    },
}
