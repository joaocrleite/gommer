const Sequelize = require('sequelize');

module.exports.sequelize = null;

module.exports.init = function init() {

    if (!this.sequelize) {

        this.sequelize = new Sequelize('goomer', 'root', '', {
            host: 'localhost',
            dialect: 'mysql',
            define: {
                timestamps: false
            }
        });

        this.sequelize
            .authenticate()
            .then((err) => {
                console.log('Connection has been established successfully.');
            })
            .catch((err) => {
                console.log('Unable to connect to the database:', err);
            });
    }

};


module.exports.User = function User() {

    this.init();

    let User =  this.sequelize.define('users', {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        age: Sequelize.INTEGER,
    });
    
    return User;

};

module.exports.Group = function Group() {

    this.init();

    let Group = this.sequelize.define('groups', {
        name: Sequelize.STRING
    });


    return Group;
};

module.exports.GroupUser = function GroupUser() {

    this.init();

    let GroupUser = this.sequelize.define('users_has_groups', {
        group_id: {
            type : Sequelize.INTEGER,
            primaryKey: true
        },
        user_id: {
            type : Sequelize.INTEGER,
            primaryKey: true
        }
    });
    

    return GroupUser;
};