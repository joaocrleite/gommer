const Sequelize = require('sequelize');

let sequelize = new Sequelize('goomer', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

sequelize
    .authenticate()
    .then((err)=> {
        console.log('Connection has been established successfully.');
    })
    .catch( (err)=> {
        console.log('Unable to connect to the database:', err);
    });

let SUser = sequelize.define('users',{
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    age: Sequelize.INTEGER,
  });

module.exports.SUser = SUser;