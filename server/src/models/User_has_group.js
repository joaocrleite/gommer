
const db = require('../services/db');

exports.table =  'users_has_groups';

exports.db = db;

exports.all = function all(callback) {
    db.all(this.table, (err, rows, fields) => {
        callback(err, rows, fields);
    });
};

exports.insert = function insert(fields, callback){

    db.insert(this.table, fields, (err, result) => {

        callback(err, result);
    });

};

exports.find = function find(id, callback){

    db.find(this.table, id, (err, result) => {
        if(result.length > 0){
            callback(err, result[0]);
        }else{
            callback(err, null);
        }
        
    });

};

exports.update = function update(id, fields, callback){

    db.update(this.table, id,fields, (err, result) => {
        callback(err, result);
    });

};

exports.remove = function remove(id, callback){

    db.remove(this.table, id, (err, result) => {
        callback(err, result);
    });

};