const Models = require('../models/Models');
const _ = require('lodash');


module.exports = function (app) {


    app.get('/users', async (req, res) => {

        let response = {
            status: false,
            count: null,
            data: []
        }

        let users = await Models.User().findAll({
            attributes: ['id', 'name', 'email', 'age']
        });

        if (users) {
            response.status = true;
            response.count = users.length;
            response.data = users;
        }


        res.send(response);

    });

    app.post('/users', async (req, res) => {

        let response = {
            status: false,
            data: null
        }

        let user = {
            name: req.body.name,
            email: req.body.email,
            age: req.body.age
        }

        user = await Models.User().create(user);

        if (user) {
            response.status = true;
            response.data = user;
        }

        res.send(response);

    });

    app.get('/users/:id', async (req, res) => {

        let response = {
            status: false,
            data: null
        }


        user = await Models.User().findById(req.params.id);

        if (user) {
            response.status = true;
            response.data = user;
        }

        res.send(response);

    });

    app.post('/users/:id', async (req, res) => {

        let response = {
            status: false,
            data: null
        }

        user = await Models.User().findById(req.params.id);

        if (user) {

            user.name = req.body.name;
            user.email = req.body.email;
            user.age = req.body.age;

            user.save();

            response.status = true;
            response.data = user;

        }

        res.send(response);

    });

    app.delete('/users/:id', async (req, res) => {

        let response = {
            status: false
        }

        user = await Models.User().findById(req.params.id);

        if (user) {
            response.status = true;
            user.destroy();
        }

        res.send(response);

    });



};