const Models = require('../models/Models');
const _ = require('lodash');

module.exports = function (app) {

    app.get('/groups', async (req, res) => {

        let response = {
            status: false,
            count: null,
            data: []
        }

        let groups = await Models.Group().findAll({
            attributes: ['id', 'name']
        });


        if (groups) {
            response.status = true;
            response.count = groups.length;
            response.data = groups;
        }

        res.send(response);

    });

    app.get('/groups-with-users', async (req, res) => {

        let response = {
            status: false,
            count: null,
            data: []
        }

        let groups = await Models.Group().findAll({
            attributes: ['id', 'name']
        });

        for (var i = 0; groups.length > i; i++) {

            let group = groups[i];

            let users_has_groups = await Models.GroupUser().findAll({
                attributes: ['user_id', 'group_id'],
                where: {
                    group_id: group.id
                }
            });


            let users = [];

            for (var y = 0; users_has_groups.length > y; y++) {

                let group_has_users = users_has_groups[y];

                let user = await Models.User().findOne({
                    attributes: ['id', 'name', 'email', 'age'],
                    where: {
                        id: group_has_users.user_id
                    }
                });

                if (user) {
                    users.push(user);
                }

            }

            group.dataValues.users = users;

        }

        if (groups) {

            response.status = true;
            response.count = groups.length;
            response.data = groups;
        }

        res.send(response);



    });

    app.post('/groups', async (req, res) => {

        let response = {
            status: false,
            data: null
        }

        let group = {
            name: req.body.name
        }

        group = await Models.Group().create(group);

        if (req.body.users) {

            _.forEach(req.body.users, async (row, index) => {

                let user_has_group = {
                    group_id: group.id,
                    user_id: row.id
                };

                user_has_group = await Models.GroupUser().findOrCreate({
                    where: user_has_group,
                    defaults: user_has_group,
                    attributes: ['user_id', 'group_id']
                });

            });

        }

        if (group) {
            response.status = true;
            response.data = group;
        }

        res.send(response);

    });

    app.get('/groups/:id', async (req, res) => {

        let response = {
            status: false,
            data: null
        }

        group = await Models.Group().findById(req.params.id);

        if (group) {


            let users_has_groups = await Models.GroupUser().findAll({
                attributes: ['user_id', 'group_id'],
                where: {
                    group_id: group.id
                }
            });


            let users = [];

            for (var y = 0; users_has_groups.length > y; y++) {

                let group_has_users = users_has_groups[y];

                let user = await Models.User().findOne({
                    attributes: ['id', 'name', 'email', 'age'],
                    where: {
                        id: group_has_users.user_id
                    }
                });

                if (user) {
                    users.push(user);
                }

            }

            group.dataValues.users = users;


            response.status = true;
            response.data = group;
        }

        res.send(response);

    });

    app.post('/groups/:id', async (req, res) => {

        let response = {
            status: false,
            data: null
        }

        user = await Models.User().findById(req.params.id);

        if (user) {

            user.name = req.body.name;
            user.email = req.body.email;
            user.age = req.body.age;

            user.save();

            response.status = true;
            response.data = user;

        }

        res.send(response);

    });

    app.delete('/groups/:id', async (req, res) => {

        let response = {
            status: false
        }

        user = await Models.User().findById(req.params.id);

        if (user) {
            response.status = true;
            user.destroy();
        }

        res.send(response);

    });


    app.delete('/groups/:id/users/:user_id', async (req, res) => {

        let response = {
            status: false,
            removed: false
        }

        let user_has_group = await Models.GroupUser().findOne({
            attributes: ['user_id', 'group_id'],
            where: {
                user_id: req.params.user_id,
                group_id: req.params.id
            }
        });

        if (user_has_group) {
            response.status = true;

            await user_has_group.destroy();

            let users = await Models.GroupUser().findAll({
                attributes: ['user_id', 'group_id'],
                where: {
                    group_id: req.params.id
                }
            });



            if (users.length < 2) {

                console.log("Removendo");

                users[0].destroy();

                group = await Models.Group().findById(req.params.id);

                group.destroy();

                response.removed = true;

            }

        }

        res.send(response);

    });



};