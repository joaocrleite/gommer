const mysql = require('mysql');


exports.config = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'goomer'
}


exports.query = async function query(query, callback) {
    
    let con = this.con();

    await con.query(query, (a, b, c) => {
        callback(a, b, c);
    });

};

exports.all = function all(table, callback) {
    
    let con = this.con();

    con.query('SELECT * FROM ' + table + ' ORDER BY id ASC', (err, rows, fields) => {
        callback(err, rows, fields);
    });
};

exports.find = function find(table, id,  callback) {
    
    let con = this.con();

    let query = con.query('SELECT * FROM ' + table + ' WHERE id = ?',[id], (err, rows, fields) => {
        callback(err, rows, fields);
    });

    console.log(query.sql, id);
};


exports.insert = function insert(table, fields, callback) {
    
    let con = this.con();

    con.query('INSERT INTO ' + table + ' SET ?', fields,  (err, result) => {
        callback(err, result);
    });


};

exports.update = function update(table, id,  fields, callback) {
    
    let con = this.con();

    con.query('UPDATE ' + table + ' SET ? WHERE id = ?', [fields, id],  (err, result) => {
        callback(err, result);
    });


};

exports.remove = function remove(table, id, callback) {
    
    let con = this.con();

    con.query('DELETE FROM ' + table + ' WHERE id = ?', [id],  (err, result) => {
        callback(err, result);
    });


};

exports.con = function con(){
    let con = mysql.createConnection(this.config);

    con.connect();

    return con;
}
