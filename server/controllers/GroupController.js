const {
    Group,
    Users_has_groups
} = require('../models');

module.exports = function (app) {


    app.get('/groups', async (req, res) => {

        let result = await Group.findAll({
            include: ['users']
        });

        res.json(result);

    });


    app.post('/groups', async (req, res) => {

        let post = {
            name: req.body.name
        };

        let postUsers = req.body.users;

        let row = await Group.create(post);

        postUsers.forEach(user => {

            Users_has_groups.create({
                user_id: user.id,
                group_id: row.id
            });

        })

        res.send(row);

    });


    app.get('/groups/:id', async (req, res) => {

        let row = await Group.findById(req.params.id, {
            include: ['users']
        });

        res.send(row);

    });


    app.post('/groups/:id', async (req, res) => {

        let post = {
            name: req.body.name
        }

        let row = await Group.findById(req.params.id);

        if (row) {
            row.name = post.name;

            row.save();
        }

        res.send(row);

    });

    app.delete('/groups/:id', async (req, res) => {

        let row = await Group.findById(req.params.id);

        if (row) {
            row.destroy();
        }

        res.send(row);

    });

};