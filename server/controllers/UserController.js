const { User } = require('../models');

module.exports = function (app) {

    app.get('/users', async (req, res) => {

        let result = await User.findAll({
            include : ['groups']
        });
        
        res.json(result);
    
    });


    app.post('/users', async (req, res) => {

        let post = {
            name: req.body.name,
            email: req.body.email,
            age: req.body.age
        }

        let row = await User.create(post);

        res.send(row);

    });


    app.get('/users/:id', async (req, res) => {

        let row = await User.findById(req.params.id, {
            include : ['groups']
        });

        res.send(row);

    });


    app.post('/users/:id', async (req, res) => {

        let post = {
            name: req.body.name,
            email: req.body.email,
            age: req.body.age
        }

        let row = await User.findById(req.params.id);

        if (row) {
            row.name = post.name;
            row.email = post.email;
            row.age = post.age;

            row.save();
        }

        res.send(row);

    });

    app.delete('/users/:id', async (req, res) => {

        let row = await User.findById(req.params.id);

        if (row) {
            row.destroy();
        }

        res.send(row);

    });


};